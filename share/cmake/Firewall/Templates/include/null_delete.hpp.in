/// Preamble {{{
//  ==========================================================================
//        @file null_delete.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-03-25 Friday 20:08:28 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-01-30 Saturday 21:15:39 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef @PP_PREFIX@NULL_DELETE_HPP_INCLUDED
# define @PP_PREFIX@NULL_DELETE_HPP_INCLUDED
//
# include <cstddef>
// clang-format on
//
// clang-format off
@NS_BEGIN@
// clang-format on
struct null_delete {
  template <class T>
  void
  operator()(T*) const {
  }
  //
  void
  operator()(std::nullptr_t) const {
  }
}; // struct null_delete
// clang-format off
@NS_END@
// clang-format on
//
// clang-format off
# endif // @PP_PREFIX@NULL_DELETE_HPP_INCLUDED
