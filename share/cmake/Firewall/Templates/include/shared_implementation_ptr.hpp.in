/// Preamble {{{
//  ==========================================================================
//        @file shared_implementation_ptr.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-03-13 Sunday 22:41:36 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-03-09 Wednesday 22:34:39 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_HPP_INCLUDED
# define @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_HPP_INCLUDED
//
# include "implementation_ptr"
# include "shared_ptr"
// clang-format on
//
// clang-format off
@NS_BEGIN@
// clang-format on
template <class T>
class shared_implementation_ptr //
    : public implementation_ptr<@PP_PREFIX@SHARED_PTR<T>> {
public:
  typedef implementation_ptr<@PP_PREFIX@SHARED_PTR<T>> base_ptr_type;
  //
public:
  using base_ptr_type::base_ptr_type;
}; // class shared_implementation_ptr
// clang-format off
@NS_END@
// clang-format on
//
// clang-format off
# define @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_TYPE(T) \
    @NS_PREFIX@shared_implementation_ptr<@PP_PREFIX@TYPE_OF(T)>
//
# define @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_ELEMENT_TYPE(T) \
    @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_TYPE(T)::element_type
//
# define @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_POINTER(T) \
    @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_TYPE(T)::pointer
//
# define @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_CONST_POINTER(T) \
    @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_TYPE(T)::const_pointer
//
# define @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_REFERENCE(T) \
    @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_TYPE(T)::reference
//
# define @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_CONST_REFERENCE(T) \
    @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_TYPE(T)::const_reference
//
# define @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_FRIEND_DECLARATION(T) \
    friend typename \
        @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_ELEMENT_TYPE(T)
//
# define @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_OBJECT_DECLARATION(T, O) \
    @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_TYPE(T) O
//
# define @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_MEMBER_DECLARATION(T, O) \
    @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_FRIEND_DECLARATION(T); \
    @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_OBJECT_DECLARATION(T, O)
// clang-format on
//
// clang-format off
# endif // @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_HPP_INCLUDED
//
# if defined(USING@PP_SUFFIX@)                           || \
     defined(USING@PP_SUFFIX@_SHARED_IMPLEMENTATION_PTR)
#   define SHARED_IMPLEMENTATION_PTR_TYPE \
      @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_TYPE
#   define SHARED_IMPLEMENTATION_PTR_ELEMENT_TYPE \
      @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_ELEMENT_TYPE
#   define SHARED_IMPLEMENTATION_PTR_POINTER \
      @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_POINTER
#   define SHARED_IMPLEMENTATION_PTR_CONST_POINTER \
      @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_CONST_POINTER
#   define SHARED_IMPLEMENTATION_PTR_REFERENCE \
      @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_REFERENCE
#   define SHARED_IMPLEMENTATION_PTR_CONST_REFERENCE \
      @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_CONST_REFERENCE
#   define SHARED_IMPLEMENTATION_PTR_FRIEND_DECLARATION \
      @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_FRIEND_DECLARATION
#   define SHARED_IMPLEMENTATION_PTR_OBJECT_DECLARATION \
      @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_OBJECT_DECLARATION
#   define SHARED_IMPLEMENTATION_PTR_MEMBER_DECLARATION \
      @PP_PREFIX@SHARED_IMPLEMENTATION_PTR_MEMBER_DECLARATION
# endif
