/// Preamble {{{
//  ==========================================================================
//        @file implementation_ptr.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-01-30 Saturday 15:23:09 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-01-25 Monday 22:22:17 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef @PP_PREFIX@IMPLEMENTATION_PTR_HPP_INCLUDED
# define @PP_PREFIX@IMPLEMENTATION_PTR_HPP_INCLUDED
//
# include "deep_const_ptr"
# include "type_of"
// clang-format on
//
// clang-format off
@NS_BEGIN@
// clang-format on
template <class PtrT>
class implementation_ptr : public deep_const_ptr<PtrT> {
public:
  typedef deep_const_ptr<PtrT> base_ptr_type;
  //
public:
  using base_ptr_type::base_ptr_type;
}; // class implementation_ptr
// clang-format off
@NS_END@
// clang-format on
//
// clang-format off
# define @PP_PREFIX@IMPLEMENTATION_PTR_TYPE(P) \
    @NS_PREFIX@implementation_ptr<@PP_PREFIX@TYPE_OF(P)>
//
# define @PP_PREFIX@IMPLEMENTATION_PTR_DELETER_TYPE(T) \
    @PP_PREFIX@IMPLEMENTATION_PTR_TYPE(T)::deleter_type
//
# define @PP_PREFIX@IMPLEMENTATION_PTR_ELEMENT_TYPE(P) \
    @PP_PREFIX@IMPLEMENTATION_PTR_TYPE(P)::element_type
//
# define @PP_PREFIX@IMPLEMENTATION_PTR_POINTER(P) \
    @PP_PREFIX@IMPLEMENTATION_PTR_TYPE(P)::pointer
//
# define @PP_PREFIX@IMPLEMENTATION_PTR_CONST_POINTER(P) \
    @PP_PREFIX@IMPLEMENTATION_PTR_TYPE(P)::const_pointer
//
# define @PP_PREFIX@IMPLEMENTATION_PTR_REFERENCE(P) \
    @PP_PREFIX@IMPLEMENTATION_PTR_TYPE(P)::reference
//
# define @PP_PREFIX@IMPLEMENTATION_PTR_CONST_REFERENCE(P) \
    @PP_PREFIX@IMPLEMENTATION_PTR_TYPE(P)::const_reference
// clang-format on
//
// clang-format off
# endif // @PP_PREFIX@IMPLEMENTATION_PTR_HPP_INCLUDED
//
# if defined(USING@PP_SUFFIX@)                    || \
     defined(USING@PP_SUFFIX@_IMPLEMENTATION_PTR)
#   define IMPLEMENTATION_PTR_TYPE \
      @PP_PREFIX@IMPLEMENTATION_PTR_TYPE
#   define IMPLEMENTATION_PTR_DELETER_TYPE \
      @PP_PREFIX@IMPLEMENTATION_PTR_DELETER_TYPE
#   define IMPLEMENTATION_PTR_ELEMENT_TYPE \
      @PP_PREFIX@IMPLEMENTATION_PTR_ELEMENT_TYPE
#   define IMPLEMENTATION_PTR_POINTER \
      @PP_PREFIX@IMPLEMENTATION_PTR_POINTER
#   define IMPLEMENTATION_PTR_CONST_POINTER \
      @PP_PREFIX@IMPLEMENTATION_PTR_CONST_POINTER
#   define IMPLEMENTATION_PTR_REFERENCE \
      @PP_PREFIX@IMPLEMENTATION_PTR_REFERENCE
#   define IMPLEMENTATION_PTR_CONST_REFERENCE \
      @PP_PREFIX@IMPLEMENTATION_PTR_CONST_REFERENCE
# endif
