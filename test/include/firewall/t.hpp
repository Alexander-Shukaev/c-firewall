/// Preamble {{{
//  ==========================================================================
//        @file t.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-11-23 Thursday 00:18:25 (+0100)
//  --------------------------------------------------------------------------
//     @created 2017-01-22 Sunday 23:58:31 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef FIREWALL_T_HPP_INCLUDED
# define FIREWALL_T_HPP_INCLUDED
//
# include <t>
//
# include <cstddef>
//
# if BOOST_VERSION < 106400
// clang-format on
T_LOG_DISABLE_BY_TYPE(::std::nullptr_t)
// clang-format off
# endif
//
# endif // FIREWALL_T_HPP_INCLUDED
