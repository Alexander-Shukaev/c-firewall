/// Preamble {{{
//  ==========================================================================
//        @file raw_deep_const_ptr_fixture.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-09 Tuesday 16:00:27 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-03-11 Friday 20:15:49 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef FIREWALL_RAW_DEEP_CONST_PTR_FIXTURE_HPP_INCLUDED
# define FIREWALL_RAW_DEEP_CONST_PTR_FIXTURE_HPP_INCLUDED
//
# include "deep_const_ptr_fixture"
//
# include <cstddef>
// clang-format on
//
namespace firewall {
template <class T = void, T* p = nullptr>
class raw_deep_const_ptr_fixture : public deep_const_ptr_fixture<T*> {
  typedef deep_const_ptr_fixture<T*> base_t;
  //
public:
  typedef T element_t;
  //
protected:
  raw_deep_const_ptr_fixture()
      : base_t(typename base_t::dcp_t(p), typename base_t::dcp_t(p)) {
  }
}; // class raw_deep_const_ptr_fixture
} // namespace firewall
//
namespace {}
//
// clang-format off
# endif // FIREWALL_RAW_DEEP_CONST_PTR_FIXTURE_HPP_INCLUDED
