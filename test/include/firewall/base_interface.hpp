/// Preamble {{{
//  ==========================================================================
//        @file base_interface.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-09 Tuesday 16:22:40 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-03-28 Monday 15:26:29 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef FIREWALL_BASE_INTERFACE_HPP_INCLUDED
# define FIREWALL_BASE_INTERFACE_HPP_INCLUDED
//
# include "call_counter"
// clang-format on
//
namespace firewall {
class base_fixture;
//
class base_interface {
public:
  explicit base_interface(base_fixture& f);
  //
private:
  call_counter _cc;
}; // class base_interface
} // namespace firewall
//
namespace {}
//
// clang-format off
# endif // FIREWALL_BASE_INTERFACE_HPP_INCLUDED
