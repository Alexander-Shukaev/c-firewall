/// Preamble {{{
//  ==========================================================================
//        @file deep_const_ptr_fixture.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-10 Wednesday 03:29:20 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-03-11 Friday 20:06:58 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef FIREWALL_DEEP_CONST_PTR_FIXTURE_HPP_INCLUDED
# define FIREWALL_DEEP_CONST_PTR_FIXTURE_HPP_INCLUDED
//
# include <firewall/deep_const_ptr>
//
# include <algorithm>
// clang-format on
//
namespace firewall {
template <class PtrT>
class deep_const_ptr_fixture {
public:
  typedef PtrT ptr_t;
  //
protected:
  // clang-format off
  typedef deep_const_ptr<ptr_t>             dcp_t;
  typedef deep_const_ptr<ptr_t> const const_dcp_t;
  // clang-format on
  //
public:
  ~deep_const_ptr_fixture();
  //
protected:
  deep_const_ptr_fixture(dcp_t&& dcp, dcp_t&& const_dcp)
      : _dcp(std::move(dcp)), _const_dcp(std::move(const_dcp)) {
  }
  //
protected:
  // clang-format off
        dcp_t       _dcp;
  const_dcp_t _const_dcp;
  // clang-format on
}; // class deep_const_ptr_fixture
//
template <class PtrT>
deep_const_ptr_fixture<PtrT>::~deep_const_ptr_fixture() = default;
} // namespace firewall
//
namespace {}
//
// clang-format off
# endif // FIREWALL_DEEP_CONST_PTR_FIXTURE_HPP_INCLUDED
