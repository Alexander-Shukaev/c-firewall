/// Preamble {{{
//  ==========================================================================
//        @file shared_interface_ptr.t.ipp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-22 Sunday 11:30:53 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-03-13 Sunday 01:56:56 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
T_TEST_SUITE(shared_interface_ptr)
//
T_TEST_CASE(base_ptr_type) {
  T_CHECK((is_same<SHARED_INTERFACE_PTR_TYPE()::base_ptr_type::base_ptr_type::
                       base_ptr_type,
                   SHARED_PTR_TYPE()>::value));
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_TYPE) {
  T_CHECK((is_same<SHARED_INTERFACE_PTR_TYPE(),
                   SHARED_INTERFACE_PTR_TYPE(void)>::value));
  T_CHECK((is_same<SHARED_INTERFACE_PTR_TYPE(),
                   SHARED_INTERFACE_PTR_TYPE((void))>::value));
  T_CHECK((is_same<SHARED_INTERFACE_PTR_TYPE(),
                   SHARED_INTERFACE_PTR_TYPE(((void)))>::value));
  //
  { SHARED_INTERFACE_PTR_TYPE(int) p; }
  { SHARED_INTERFACE_PTR_TYPE((int)) p; }
  { SHARED_INTERFACE_PTR_TYPE(((int))) p; }
  //
  { SHARED_INTERFACE_PTR_TYPE((int)) p(0, null_delete()); }
  { SHARED_INTERFACE_PTR_TYPE(((int))) p(0, null_delete()); }
  { SHARED_INTERFACE_PTR_TYPE((((int)))) p(0, null_delete()); }
  //
  { SHARED_INTERFACE_PTR_TYPE((int)) p(nullptr, null_delete()); }
  { SHARED_INTERFACE_PTR_TYPE(((int))) p(nullptr, null_delete()); }
  { SHARED_INTERFACE_PTR_TYPE((((int)))) p(nullptr, null_delete()); }
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_ELEMENT_TYPE) {
  T_CHECK( //
      (is_same<int, SHARED_INTERFACE_PTR_ELEMENT_TYPE(int)>::value));
  T_CHECK( //
      (is_same<int, SHARED_INTERFACE_PTR_ELEMENT_TYPE((int))>::value));
  T_CHECK( //
      (is_same<int, SHARED_INTERFACE_PTR_ELEMENT_TYPE(((int)))>::value));
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_POINTER) {
  T_CHECK(           //
      (is_same<int*, //
               SHARED_INTERFACE_PTR_POINTER(int)>::value));
  T_CHECK(           //
      (is_same<int*, //
               SHARED_INTERFACE_PTR_POINTER((int))>::value));
  T_CHECK(           //
      (is_same<int*, //
               SHARED_INTERFACE_PTR_POINTER(((int)))>::value));
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_CONST_POINTER) {
  T_CHECK(                 //
      (is_same<int const*, //
               SHARED_INTERFACE_PTR_CONST_POINTER(int)>::value));
  T_CHECK(                 //
      (is_same<int const*, //
               SHARED_INTERFACE_PTR_CONST_POINTER((int))>::value));
  T_CHECK(                 //
      (is_same<int const*, //
               SHARED_INTERFACE_PTR_CONST_POINTER(((int)))>::value));
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_REFERENCE) {
  T_CHECK(           //
      (is_same<int&, //
               SHARED_INTERFACE_PTR_REFERENCE(int)>::value));
  T_CHECK(           //
      (is_same<int&, //
               SHARED_INTERFACE_PTR_REFERENCE((int))>::value));
  T_CHECK(           //
      (is_same<int&, //
               SHARED_INTERFACE_PTR_REFERENCE(((int)))>::value));
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_CONST_REFERENCE) {
  T_CHECK(                 //
      (is_same<int const&, //
               SHARED_INTERFACE_PTR_CONST_REFERENCE(int)>::value));
  T_CHECK(                 //
      (is_same<int const&, //
               SHARED_INTERFACE_PTR_CONST_REFERENCE((int))>::value));
  T_CHECK(                 //
      (is_same<int const&, //
               SHARED_INTERFACE_PTR_CONST_REFERENCE(((int)))>::value));
}
//
T_TEST_SUITE_END
//
T_TEST_SUITE_BY_FIXTURE(shared_interface_ptr, base_fixture)
//
T_TEST_CASE(SHARED_INTERFACE_PTR_TYPE_with_default_deleter) {
  {
    class implementation {
    public:
      explicit implementation(base_fixture& f) : _i(new base_interface(f)) {
      }
      //
    private:
      SHARED_INTERFACE_PTR_TYPE(base_interface) _i;
    } i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_TYPE_with_default_delete) {
  {
    class implementation {
    public:
      explicit implementation(base_fixture& f)
          : _i(new base_interface(f), default_delete<base_interface>()) {
      }
      //
    private:
      SHARED_INTERFACE_PTR_TYPE(base_interface) _i;
    } i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_TYPE_with_null_delete) {
  {
    class implementation {
    public:
      explicit implementation(base_fixture& f)
          : _i(new base_interface(f), null_delete()) {
      }
      //
    private:
      SHARED_INTERFACE_PTR_TYPE(base_interface) _i;
    } i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_FRIEND_DECLARATION_with_default_deleter) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this)) {
          _i->check();
        }
        //
      private:
        SHARED_INTERFACE_PTR_FRIEND_DECLARATION(interface);
        SHARED_INTERFACE_PTR_TYPE(interface) _i;
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_FRIEND_DECLARATION_with_default_delete) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this), default_delete<interface>()) {
          _i->check();
        }
        //
      private:
        SHARED_INTERFACE_PTR_FRIEND_DECLARATION(interface);
        SHARED_INTERFACE_PTR_TYPE(interface) _i;
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_FRIEND_DECLARATION_with_null_delete) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this), null_delete()) {
          _i->check();
        }
        //
      private:
        SHARED_INTERFACE_PTR_FRIEND_DECLARATION(interface);
        SHARED_INTERFACE_PTR_TYPE(interface) _i;
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_OBJECT_DECLARATION_with_default_deleter) {
  {
    SHARED_INTERFACE_PTR_OBJECT_DECLARATION(base_interface, i)
    (new base_interface(*this));
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_OBJECT_DECLARATION_with_default_delete) {
  {
    SHARED_INTERFACE_PTR_OBJECT_DECLARATION(base_interface, i)
    (new base_interface(*this), default_delete<base_interface>());
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_OBJECT_DECLARATION_with_null_delete) {
  {
    SHARED_INTERFACE_PTR_OBJECT_DECLARATION(base_interface, i)
    (new base_interface(*this), null_delete());
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_MEMBER_DECLARATION_with_default_deleter) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this)) {
          _i->check();
        }
        //
      private:
        SHARED_INTERFACE_PTR_MEMBER_DECLARATION(interface, _i);
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_MEMBER_DECLARATION_with_default_delete) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this), default_delete<interface>()) {
          _i->check();
        }
        //
      private:
        SHARED_INTERFACE_PTR_MEMBER_DECLARATION(interface, _i);
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(SHARED_INTERFACE_PTR_MEMBER_DECLARATION_with_null_delete) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this), null_delete()) {
          _i->check();
        }
        //
      private:
        SHARED_INTERFACE_PTR_MEMBER_DECLARATION(interface, _i);
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_SUITE_END
