/// Preamble {{{
//  ==========================================================================
//        @file shared_interface_ptr.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-23 Monday 00:02:37 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-03-13 Sunday 01:56:56 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <firewall/base_fixture>
# include <firewall/base_interface>
//
# include <firewall/null_delete>
# define  FIREWALL_USE_BOOST_DEFAULT_DELETE 0
# define  USING_FIREWALL_DEFAULT_DELETE
# include <firewall/default_delete>
# define  FIREWALL_USE_BOOST_SHARED_PTR     0
# define  USING_FIREWALL_SHARED_INTERFACE_PTR
# define  USING_FIREWALL_SHARED_PTR
# include <firewall/shared_interface_ptr>
//
# include <firewall/t>
//
# include <type_traits>
// clang-format on
//
using std::is_same;
//
namespace firewall {
namespace {
T_TEST_SUITE(shared_interface_ptr)
//
T_TEST_CASE(default_delete) {
  T_CHECK((::is_same<::std::default_delete<void>, //
                     default_delete<void>>::value));
}
//
T_TEST_CASE(DEFAULT_DELETE) {
  T_CHECK((::is_same<::std::default_delete<void>, //
                     DEFAULT_DELETE<void>>::value));
}
//
T_TEST_CASE(DEFAULT_DELETE_TYPE) {
  T_CHECK((::is_same<::std::default_delete<void>, //
                     DEFAULT_DELETE_TYPE()>::value));
}
//
T_TEST_CASE(shared_ptr) {
  T_CHECK((::is_same<::std::shared_ptr<void>, //
                     shared_ptr<void>>::value));
}
//
T_TEST_CASE(SHARED_PTR) {
  T_CHECK((::is_same<::std::shared_ptr<void>, //
                     SHARED_PTR<void>>::value));
}
//
T_TEST_CASE(SHARED_PTR_TYPE) {
  T_CHECK((::is_same<::std::shared_ptr<void>, //
                     SHARED_PTR_TYPE()>::value));
}
//
T_TEST_SUITE_END
//
// clang-format off
# include "../shared_interface_ptr.t.ipp"
// clang-format on
} // namespace
} // namespace firewall
