/// Preamble {{{
//  ==========================================================================
//        @file unique_deep_const_ptr.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-23 Monday 00:02:35 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-01-29 Friday 19:09:23 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# define  FIREWALL_USE_BOOST_UNIQUE_PTR 1
# include <firewall/unique_deep_const_ptr_fixture>
//
# include <firewall/t>
//
# include <boost/type_traits/is_const.hpp>
# include <boost/type_traits/is_constructible.hpp>
# include <boost/type_traits/is_same.hpp>
# include <boost/type_traits/remove_pointer.hpp>
# include <boost/type_traits/remove_reference.hpp>
// clang-format on
//
using boost::is_const;
using boost::is_constructible;
using boost::is_same;
using boost::remove_pointer;
using boost::remove_reference;
//
namespace firewall {
namespace {
typedef unique_deep_const_ptr_fixture<> fixture_t;
//
// clang-format off
typedef fixture_t::element_t element_t;
typedef fixture_t::    ptr_t     ptr_t;
//
static_assert(is_same<element_t,                   void        >::value, "");
static_assert(is_same<    ptr_t, boost::unique_ptr<void,
                                                   null_delete>>::value, "");
//
T_TEST_SUITE_BY_FIXTURE(unique_deep_const_ptr, fixture_t)
//
# define WITH_OPERATOR_ARROW         0
# define WITH_OPERATOR_INDIRECTION   0
# define WITH_OPERATOR_EQUAL         1
# define WITH_OPERATOR_NOT_EQUAL     0
# define WITH_OPERATOR_LESS_EQUAL    0
# define WITH_OPERATOR_GREATER_EQUAL 0
# define WITH_OPERATOR_LESS          0
# define WITH_OPERATOR_GREATER       0
# define WITH_DELETE                 0
# include "../../deep_const_ptr.t.ipp"
//
T_TEST_SUITE_END
// clang-format on
} // namespace
} // namespace firewall
