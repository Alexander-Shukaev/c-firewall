/// Preamble {{{
//  ==========================================================================
//        @file raw_implementation_ptr.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-23 Monday 00:02:34 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-03-13 Sunday 01:56:56 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <firewall/base_fixture>
# include <firewall/base_implementation>
//
# define  USING_FIREWALL_RAW_IMPLEMENTATION_PTR
# define  USING_FIREWALL_RAW_PTR
# include <firewall/raw_implementation_ptr>
//
# include <firewall/t>
//
# include <boost/type_traits/is_same.hpp>
// clang-format on
//
using boost::is_same;
//
namespace firewall {
namespace {
T_TEST_SUITE(raw_implementation_ptr)
//
T_TEST_CASE(RAW_PTR_TYPE) {
  T_CHECK((::is_same<void*, RAW_PTR_TYPE()>::value));
}
//
T_TEST_SUITE_END
//
// clang-format off
# include "../raw_implementation_ptr.t.ipp"
// clang-format on
} // namespace
} // namespace firewall
