/// Preamble {{{
//  ==========================================================================
//        @file raw_interface_ptr.t.ipp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-22 Sunday 11:30:52 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-03-13 Sunday 01:56:56 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
T_TEST_SUITE(raw_interface_ptr)
//
T_TEST_CASE(RAW_INTERFACE_PTR_TYPE) {
  T_CHECK((is_same<RAW_INTERFACE_PTR_TYPE(),
                   RAW_INTERFACE_PTR_TYPE(void)>::value));
  T_CHECK((is_same<RAW_INTERFACE_PTR_TYPE(),
                   RAW_INTERFACE_PTR_TYPE((void))>::value));
  T_CHECK((is_same<RAW_INTERFACE_PTR_TYPE(),
                   RAW_INTERFACE_PTR_TYPE(((void)))>::value));
  //
  { RAW_INTERFACE_PTR_TYPE(int) p; }
  { RAW_INTERFACE_PTR_TYPE((int)) p; }
  { RAW_INTERFACE_PTR_TYPE(((int))) p; }
  //
  { RAW_INTERFACE_PTR_TYPE((int)) p(0); }
  { RAW_INTERFACE_PTR_TYPE(((int))) p(0); }
  { RAW_INTERFACE_PTR_TYPE((((int)))) p(0); }
  //
  { RAW_INTERFACE_PTR_TYPE((int)) p(nullptr); }
  { RAW_INTERFACE_PTR_TYPE(((int))) p(nullptr); }
  { RAW_INTERFACE_PTR_TYPE((((int)))) p(nullptr); }
}
//
T_TEST_CASE(RAW_INTERFACE_PTR_ELEMENT_TYPE) {
  T_CHECK( //
      (is_same<int, RAW_INTERFACE_PTR_ELEMENT_TYPE(int)>::value));
  T_CHECK( //
      (is_same<int, RAW_INTERFACE_PTR_ELEMENT_TYPE((int))>::value));
  T_CHECK( //
      (is_same<int, RAW_INTERFACE_PTR_ELEMENT_TYPE(((int)))>::value));
}
//
T_TEST_CASE(RAW_INTERFACE_PTR_POINTER) {
  T_CHECK(           //
      (is_same<int*, //
               RAW_INTERFACE_PTR_POINTER(int)>::value));
  T_CHECK(           //
      (is_same<int*, //
               RAW_INTERFACE_PTR_POINTER((int))>::value));
  T_CHECK(           //
      (is_same<int*, //
               RAW_INTERFACE_PTR_POINTER(((int)))>::value));
}
//
T_TEST_CASE(RAW_INTERFACE_PTR_CONST_POINTER) {
  T_CHECK(                 //
      (is_same<int const*, //
               RAW_INTERFACE_PTR_CONST_POINTER(int)>::value));
  T_CHECK(                 //
      (is_same<int const*, //
               RAW_INTERFACE_PTR_CONST_POINTER((int))>::value));
  T_CHECK(                 //
      (is_same<int const*, //
               RAW_INTERFACE_PTR_CONST_POINTER(((int)))>::value));
}
//
T_TEST_CASE(RAW_INTERFACE_PTR_REFERENCE) {
  T_CHECK(           //
      (is_same<int&, //
               RAW_INTERFACE_PTR_REFERENCE(int)>::value));
  T_CHECK(           //
      (is_same<int&, //
               RAW_INTERFACE_PTR_REFERENCE((int))>::value));
  T_CHECK(           //
      (is_same<int&, //
               RAW_INTERFACE_PTR_REFERENCE(((int)))>::value));
}
//
T_TEST_CASE(RAW_INTERFACE_PTR_CONST_REFERENCE) {
  T_CHECK(                 //
      (is_same<int const&, //
               RAW_INTERFACE_PTR_CONST_REFERENCE(int)>::value));
  T_CHECK(                 //
      (is_same<int const&, //
               RAW_INTERFACE_PTR_CONST_REFERENCE((int))>::value));
  T_CHECK(                 //
      (is_same<int const&, //
               RAW_INTERFACE_PTR_CONST_REFERENCE(((int)))>::value));
}
//
T_TEST_SUITE_END
//
T_TEST_SUITE_BY_FIXTURE(raw_interface_ptr, base_fixture)
//
T_TEST_CASE(RAW_INTERFACE_PTR_TYPE_without_deleter) {
  {
    class implementation {
    public:
      explicit implementation(base_fixture& f) : _i(new base_interface(f)) {
      }
      //
    private:
      RAW_INTERFACE_PTR_TYPE(base_interface) _i;
    } i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(RAW_INTERFACE_PTR_FRIEND_DECLARATION_without_deleter) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this)) {
          _i->check();
        }
        //
      private:
        RAW_INTERFACE_PTR_FRIEND_DECLARATION(interface);
        RAW_INTERFACE_PTR_TYPE(interface) _i;
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(RAW_INTERFACE_PTR_OBJECT_DECLARATION_without_deleter) {
  {
    RAW_INTERFACE_PTR_OBJECT_DECLARATION(base_interface, i)
    (new base_interface(*this));
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(RAW_INTERFACE_PTR_MEMBER_DECLARATION_without_deleter) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this)) {
          _i->check();
        }
        //
      private:
        RAW_INTERFACE_PTR_MEMBER_DECLARATION(interface, _i);
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_SUITE_END
