/// Preamble {{{
//  ==========================================================================
//        @file base_fixture.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-23 Monday 00:02:34 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-03-28 Monday 14:16:56 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <firewall/base_fixture>
//
# include <firewall/t>
// clang-format on
//
namespace firewall {
base_fixture::base_fixture()
    : constructor_call_count(0), destructor_call_count(0) {
  // clang-format off
  T_REQUIRE_EQ(0, constructor_call_count);
  T_REQUIRE_EQ(0,  destructor_call_count);
  // clang-format on
}
} // namespace firewall
