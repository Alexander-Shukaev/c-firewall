/// Preamble {{{
//  ==========================================================================
//        @file deep_const_ptr.t.ipp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-22 Sunday 23:42:17 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-01-31 Sunday 17:15:03 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
template <class F, class T>
struct is_explicitly_convertible : public is_constructible<T, F> {};
//
T_TEST_CASE(element_type) {
  // clang-format off
  T_CHECK((is_same<      dcp_t::element_type, element_t>::value));
  T_CHECK((is_same<const_dcp_t::element_type, element_t>::value));
  T_CHECK((is_same<      dcp_t::element_type,
                   const_dcp_t::element_type>::value));
  //
  T_CHECK(!is_const<      dcp_t::element_type>::value);
  T_CHECK(!is_const<const_dcp_t::element_type>::value);
  // clang-format on
}
//
T_TEST_CASE(const_element_type) {
  // clang-format off
  T_CHECK((is_same<      dcp_t::element_type const, element_t const>::value));
  T_CHECK((is_same<const_dcp_t::element_type const, element_t const>::value));
  T_CHECK((is_same<      dcp_t::element_type const,
                   const_dcp_t::element_type const>::value));
  //
  T_CHECK(is_const<      dcp_t::element_type const>::value);
  T_CHECK(is_const<const_dcp_t::element_type const>::value);
  // clang-format on
}
//
T_TEST_CASE(pointer) {
  // clang-format off
  T_CHECK((is_same<remove_pointer<      dcp_t::pointer>::type,
                                        dcp_t::element_type>::value));
  T_CHECK((is_same<remove_pointer<const_dcp_t::pointer>::type,
                                  const_dcp_t::element_type>::value));
  T_CHECK((is_same<                     dcp_t::pointer,
                                  const_dcp_t::pointer>::value));
  //
  T_CHECK(!is_const<remove_pointer<      dcp_t::pointer>::type>::value);
  T_CHECK(!is_const<remove_pointer<const_dcp_t::pointer>::type>::value);
  // clang-format on
}
//
T_TEST_CASE(const_pointer) {
  // clang-format off
  T_CHECK((is_same<remove_pointer<      dcp_t::const_pointer>::type,
                                        dcp_t::element_type const>::value));
  T_CHECK((is_same<remove_pointer<const_dcp_t::const_pointer>::type,
                                  const_dcp_t::element_type const>::value));
  T_CHECK((is_same<                     dcp_t::const_pointer,
                                  const_dcp_t::const_pointer>::value));
  //
  T_CHECK(is_const<remove_pointer<      dcp_t::const_pointer>::type>::value);
  T_CHECK(is_const<remove_pointer<const_dcp_t::const_pointer>::type>::value);
  // clang-format on
}
//
T_TEST_CASE(reference) {
  // clang-format off
  T_CHECK((is_same<remove_reference<      dcp_t::reference>::type,
                                          dcp_t::element_type>::value));
  T_CHECK((is_same<remove_reference<const_dcp_t::reference>::type,
                                    const_dcp_t::element_type>::value));
  T_CHECK((is_same<                       dcp_t::reference,
                                    const_dcp_t::reference>::value));
  //
  T_CHECK(!is_const<remove_reference<      dcp_t::reference>::type>::value);
  T_CHECK(!is_const<remove_reference<const_dcp_t::reference>::type>::value);
  // clang-format on
}
//
T_TEST_CASE(const_reference) {
  // clang-format off
  T_CHECK((is_same<remove_reference<      dcp_t::const_reference>::type,
                                          dcp_t::element_type const>::value));
  T_CHECK((is_same<remove_reference<const_dcp_t::const_reference>::type,
                                    const_dcp_t::element_type const>::value));
  T_CHECK((is_same<                       dcp_t::const_reference,
                                    const_dcp_t::const_reference>::value));
  //
  T_CHECK(
      is_const<remove_reference<      dcp_t::const_reference>::type>::value);
  T_CHECK(
      is_const<remove_reference<const_dcp_t::const_reference>::type>::value);
  // clang-format on
}
//
T_TEST_CASE(get) {
  using get_t = remove_pointer<decltype(_dcp.get())>::type;
  //
  // clang-format off
  T_CHECK(( is_same<get_t, dcp_t::element_type      >::value));
  T_CHECK((!is_same<get_t, dcp_t::element_type const>::value));
  //
  T_CHECK(!is_const<get_t>::value);
  // clang-format on
}
//
T_TEST_CASE(const_get) {
  using const_get_t = remove_pointer<decltype(_const_dcp.get())>::type;
  //
  // clang-format off
  T_CHECK((!is_same<const_get_t, dcp_t::element_type      >::value));
  T_CHECK(( is_same<const_get_t, dcp_t::element_type const>::value));
  //
  T_CHECK(is_const<const_get_t>::value);
  // clang-format on
}
//
T_TEST_CASE(void_get) {
  using get_t = remove_pointer<decltype(_dcp.get<void>())>::type;
  //
  // clang-format off
  T_CHECK(( is_same<get_t, void      >::value));
  T_CHECK((!is_same<get_t, void const>::value));
  //
  T_CHECK(!is_const<get_t>::value);
  // clang-format on
}
//
T_TEST_CASE(const_void_get) {
  using const_get_t = remove_pointer<decltype(_const_dcp.get<void>())>::type;
  //
  // clang-format off
  T_CHECK((!is_same<const_get_t, void      >::value));
  T_CHECK(( is_same<const_get_t, void const>::value));
  //
  T_CHECK(is_const<const_get_t>::value);
  // clang-format on
}
//
T_TEST_CASE(float_get) {
  using get_t = remove_pointer<decltype(_dcp.get<float>())>::type;
  //
  // clang-format off
  T_CHECK(( is_same<get_t, float      >::value));
  T_CHECK((!is_same<get_t, float const>::value));
  //
  T_CHECK(!is_const<get_t>::value);
  // clang-format on
}
//
T_TEST_CASE(const_float_get) {
  using const_get_t = remove_pointer<decltype(_const_dcp.get<float>())>::type;
  //
  // clang-format off
  T_CHECK((!is_same<const_get_t, float      >::value));
  T_CHECK(( is_same<const_get_t, float const>::value));
  //
  T_CHECK(is_const<const_get_t>::value);
  // clang-format on
}
//
T_TEST_CASE(const_operator_bool) {
  // clang-format off
  T_CHECK((is_explicitly_convertible<      dcp_t, bool>::value));
  T_CHECK((is_explicitly_convertible<const_dcp_t, bool>::value));
  //
  T_CHECK_EQ(bool(      _dcp), bool(      _dcp.get()));
  T_CHECK_EQ(bool(_const_dcp), bool(_const_dcp.get()));
  // clang-format on
}
//
T_TEST_CASE(const_operator_logical_not) {
  // clang-format off
  T_CHECK_EQ(!      _dcp, !      _dcp.get());
  T_CHECK_EQ(!_const_dcp, !_const_dcp.get());
  // clang-format on
}
//
#if WITH_OPERATOR_ARROW
T_TEST_CASE(operator_arrow) {
  using testee_t = remove_reference<decltype(_dcp->ref())>::type;
  //
  // clang-format off
  T_CHECK(( is_same<testee_t, dcp_t::element_type      >::value));
  T_CHECK((!is_same<testee_t, dcp_t::element_type const>::value));
  //
  T_CHECK(!is_const<testee_t>::value);
  // clang-format on
}
//
T_TEST_CASE(const_operator_arrow) {
  using const_testee_t = remove_reference<decltype(_const_dcp->ref())>::type;
  //
  // clang-format off
  T_CHECK((!is_same<const_testee_t, dcp_t::element_type      >::value));
  T_CHECK(( is_same<const_testee_t, dcp_t::element_type const>::value));
  //
  T_CHECK(is_const<const_testee_t>::value);
  // clang-format on
}
#endif
//
#if WITH_OPERATOR_INDIRECTION
T_TEST_CASE(operator_indirection) {
  using testee_t = remove_reference<decltype((*_dcp).ref())>::type;
  //
  // clang-format off
  T_CHECK(( is_same<testee_t, dcp_t::element_type      >::value));
  T_CHECK((!is_same<testee_t, dcp_t::element_type const>::value));
  //
  T_CHECK(!is_const<testee_t>::value);
  // clang-format on
}
//
T_TEST_CASE(const_operator_indirection) {
  using const_testee_t =
      remove_reference<decltype((*_const_dcp).ref())>::type;
  //
  // clang-format off
  T_CHECK((!is_same<const_testee_t, dcp_t::element_type      >::value));
  T_CHECK(( is_same<const_testee_t, dcp_t::element_type const>::value));
  //
  T_CHECK(is_const<const_testee_t>::value);
  // clang-format on
}
#endif
//
#if WITH_OPERATOR_EQUAL
T_TEST_CASE(const_operator_equal) {
  dcp_t dcp_1(nullptr);
  dcp_t dcp_2(nullptr);
  //
  T_CHECK_EQ(dcp_1, dcp_2);
  T_CHECK_EQ(dcp_2, dcp_1);
  //
  T_CHECK_EQ(dcp_1, nullptr);
  T_CHECK_EQ(dcp_2, nullptr);
  //
  T_CHECK_EQ(nullptr, dcp_1);
  T_CHECK_EQ(nullptr, dcp_2);
  //
  // clang-format off
  T_CHECK_EQ(      _dcp, _dcp);
  T_CHECK_EQ(_const_dcp, _const_dcp);
  T_CHECK_EQ(_const_dcp, _dcp);
  T_CHECK_EQ(      _dcp, _const_dcp);
  // clang-format on
}
#endif
//
#if WITH_OPERATOR_NOT_EQUAL
T_TEST_CASE(const_operator_not_equal) {
  dcp_t dcp_1(new dcp_t::element_type);
  dcp_t dcp_2(new dcp_t::element_type);
  //
  T_CHECK_NE(dcp_1, dcp_2);
  T_CHECK_NE(dcp_2, dcp_1);
  //
  T_CHECK_NE(dcp_1, nullptr);
  T_CHECK_NE(dcp_2, nullptr);
  //
  T_CHECK_NE(nullptr, dcp_1);
  T_CHECK_NE(nullptr, dcp_2);
//
#if WITH_DELETE
  delete dcp_1.get();
  delete dcp_2.get();
#endif
}
#endif
//
#if WITH_OPERATOR_LESS_EQUAL
T_TEST_CASE(const_operator_less_equal) {
  dcp_t dcp_1(new dcp_t::element_type);
  dcp_t dcp_2(new dcp_t::element_type);
  //
  T_CHECK_LE(nullptr, dcp_1);
  T_CHECK_LE(nullptr, dcp_2);
  //
  T_CHECK_LE(dcp_1, dcp_1);
  T_CHECK_LE(dcp_2, dcp_2);
  //
  if (dcp_1 <= dcp_2)
    T_CHECK_LE(dcp_1, dcp_2);
  else
    T_CHECK_LE(dcp_2, dcp_1);
//
#if WITH_DELETE
  delete dcp_1.get();
  delete dcp_2.get();
#endif
}
#endif
//
#if WITH_OPERATOR_GREATER_EQUAL
T_TEST_CASE(const_operator_greater_equal) {
  dcp_t dcp_1(new dcp_t::element_type);
  dcp_t dcp_2(new dcp_t::element_type);
  //
  T_CHECK_GE(dcp_1, nullptr);
  T_CHECK_GE(dcp_2, nullptr);
  //
  T_CHECK_GE(dcp_1, dcp_1);
  T_CHECK_GE(dcp_2, dcp_2);
  //
  if (dcp_1 >= dcp_2)
    T_CHECK_GE(dcp_1, dcp_2);
  else
    T_CHECK_GE(dcp_2, dcp_1);
//
#if WITH_DELETE
  delete dcp_1.get();
  delete dcp_2.get();
#endif
}
#endif
//
#if WITH_OPERATOR_LESS
T_TEST_CASE(const_operator_less) {
  dcp_t dcp_1(new dcp_t::element_type);
  dcp_t dcp_2(new dcp_t::element_type);
  //
  T_CHECK_LT(nullptr, dcp_1);
  T_CHECK_LT(nullptr, dcp_2);
  //
  if (dcp_1 < dcp_2)
    T_CHECK_LT(dcp_1, dcp_2);
  else
    T_CHECK_LT(dcp_2, dcp_1);
//
#if WITH_DELETE
  delete dcp_1.get();
  delete dcp_2.get();
#endif
}
#endif
//
#if WITH_OPERATOR_GREATER
T_TEST_CASE(const_operator_greater) {
  dcp_t dcp_1(new dcp_t::element_type);
  dcp_t dcp_2(new dcp_t::element_type);
  //
  T_CHECK_GT(dcp_1, nullptr);
  T_CHECK_GT(dcp_2, nullptr);
  //
  if (dcp_1 > dcp_2)
    T_CHECK_GT(dcp_1, dcp_2);
  else
    T_CHECK_GT(dcp_2, dcp_1);
//
#if WITH_DELETE
  delete dcp_1.get();
  delete dcp_2.get();
#endif
}
#endif
