### Preamble {{{
##  ==========================================================================
##        @file CMakeLists.txt
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:54:04 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-01-29 Friday 19:12:11 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
file(GLOB CPP_FILES *.cpp)
##
add(LIB ${PROJECT}.t OBJECT ${CPP_FILES}
  TARGET
  COMPILE OPTS PRIVATE ${PROJECT_OPTIONS}
  INCLUDE DIRS PRIVATE ${PROJECT_INCLUDE_DIRS}
  BTEST)
##
macro(firewall_add_test)
  get_filename_component(_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
  ##
  file(GLOB_RECURSE T_CPP_FILES *.t.cpp)
  ##
  add(EXE ${PROJECT}.${_NAME}.t ${T_CPP_FILES} $<TARGET_OBJECTS:${PROJECT}.t>
    TARGET
    SYMLINK  REQUIRED CASE LOWER
    INCLUDE  DIRS PRIVATE ${PROJECT_INCLUDE_DIRS}
    LINK     LIBS PRIVATE ${PROJECT}
    GENERATE VERSION_SCRIPT REQUIRED LINK
    CHECK    HARDENING NO_FFS
    BTEST    check memcheck)
endmacro()
##
add_subdirectory(boost)
add_subdirectory(std)
