/// Preamble {{{
//  ==========================================================================
//        @file call_counter.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-09 Tuesday 16:20:57 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-03-28 Monday 14:27:32 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <firewall/call_counter>
//
# include <firewall/base_fixture>
// clang-format on
//
namespace firewall {
call_counter::~call_counter() {
  _f.destructor_call_count++;
}
//
call_counter::call_counter(base_fixture& f) : _f(f) {
  _f.constructor_call_count++;
}
} // namespace firewall
