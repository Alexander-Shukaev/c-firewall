/// Preamble {{{
//  ==========================================================================
//        @file unique_interface_ptr.t.ipp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-22 Sunday 11:30:53 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-03-13 Sunday 01:56:56 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
T_TEST_SUITE(unique_interface_ptr)
//
T_TEST_CASE(base_ptr_type) {
  T_CHECK((is_same<UNIQUE_INTERFACE_PTR_TYPE()::base_ptr_type::base_ptr_type::
                       base_ptr_type,
                   UNIQUE_PTR_TYPE()>::value));
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_TYPE) {
  T_CHECK((is_same<UNIQUE_INTERFACE_PTR_TYPE(),
                   UNIQUE_INTERFACE_PTR_TYPE(void)>::value));
  T_CHECK((is_same<UNIQUE_INTERFACE_PTR_TYPE(),
                   UNIQUE_INTERFACE_PTR_TYPE((void))>::value));
  T_CHECK((is_same<UNIQUE_INTERFACE_PTR_TYPE(),
                   UNIQUE_INTERFACE_PTR_TYPE(((void)))>::value));
  //
  { UNIQUE_INTERFACE_PTR_TYPE(int) p; }
  { UNIQUE_INTERFACE_PTR_TYPE((int)) p; }
  { UNIQUE_INTERFACE_PTR_TYPE(((int))) p; }
  //
  { UNIQUE_INTERFACE_PTR_TYPE((int, null_delete)) p; }
  { UNIQUE_INTERFACE_PTR_TYPE(((int, null_delete))) p; }
  { UNIQUE_INTERFACE_PTR_TYPE((((int, null_delete)))) p; }
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_DELETER_TYPE) {
  T_CHECK(                          //
      (is_same<DEFAULT_DELETE<int>, //
               UNIQUE_INTERFACE_PTR_DELETER_TYPE(int)>::value));
  T_CHECK(                          //
      (is_same<DEFAULT_DELETE<int>, //
               UNIQUE_INTERFACE_PTR_DELETER_TYPE((int))>::value));
  T_CHECK(                          //
      (is_same<DEFAULT_DELETE<int>, //
               UNIQUE_INTERFACE_PTR_DELETER_TYPE(((int)))>::value));
  //
  T_CHECK((is_same<null_delete,                       //
                   UNIQUE_INTERFACE_PTR_DELETER_TYPE( //
                       (int, null_delete))>::value));
  T_CHECK((is_same<null_delete,                       //
                   UNIQUE_INTERFACE_PTR_DELETER_TYPE( //
                       ((int, null_delete)))>::value));
  T_CHECK((is_same<null_delete,                       //
                   UNIQUE_INTERFACE_PTR_DELETER_TYPE( //
                       (((int, null_delete))))>::value));
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_ELEMENT_TYPE) {
  T_CHECK( //
      (is_same<int, UNIQUE_INTERFACE_PTR_ELEMENT_TYPE(int)>::value));
  T_CHECK( //
      (is_same<int, UNIQUE_INTERFACE_PTR_ELEMENT_TYPE((int))>::value));
  T_CHECK( //
      (is_same<int, UNIQUE_INTERFACE_PTR_ELEMENT_TYPE(((int)))>::value));
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_POINTER) {
  T_CHECK(           //
      (is_same<int*, //
               UNIQUE_INTERFACE_PTR_POINTER(int)>::value));
  T_CHECK(           //
      (is_same<int*, //
               UNIQUE_INTERFACE_PTR_POINTER((int))>::value));
  T_CHECK(           //
      (is_same<int*, //
               UNIQUE_INTERFACE_PTR_POINTER(((int)))>::value));
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_CONST_POINTER) {
  T_CHECK(                 //
      (is_same<int const*, //
               UNIQUE_INTERFACE_PTR_CONST_POINTER(int)>::value));
  T_CHECK(                 //
      (is_same<int const*, //
               UNIQUE_INTERFACE_PTR_CONST_POINTER((int))>::value));
  T_CHECK(                 //
      (is_same<int const*, //
               UNIQUE_INTERFACE_PTR_CONST_POINTER(((int)))>::value));
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_REFERENCE) {
  T_CHECK(           //
      (is_same<int&, //
               UNIQUE_INTERFACE_PTR_REFERENCE(int)>::value));
  T_CHECK(           //
      (is_same<int&, //
               UNIQUE_INTERFACE_PTR_REFERENCE((int))>::value));
  T_CHECK(           //
      (is_same<int&, //
               UNIQUE_INTERFACE_PTR_REFERENCE(((int)))>::value));
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_CONST_REFERENCE) {
  T_CHECK(                 //
      (is_same<int const&, //
               UNIQUE_INTERFACE_PTR_CONST_REFERENCE(int)>::value));
  T_CHECK(                 //
      (is_same<int const&, //
               UNIQUE_INTERFACE_PTR_CONST_REFERENCE((int))>::value));
  T_CHECK(                 //
      (is_same<int const&, //
               UNIQUE_INTERFACE_PTR_CONST_REFERENCE(((int)))>::value));
}
//
T_TEST_SUITE_END
//
T_TEST_SUITE_BY_FIXTURE(unique_interface_ptr, base_fixture)
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_TYPE_with_default_deleter) {
  {
    class implementation {
    public:
      explicit implementation(base_fixture& f) : _i(new base_interface(f)) {
      }
      //
    private:
      UNIQUE_INTERFACE_PTR_TYPE(base_interface) _i;
    } i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_TYPE_with_default_delete) {
  {
    class implementation {
    public:
      explicit implementation(base_fixture& f) : _i(new base_interface(f)) {
      }
      //
    private:
      UNIQUE_INTERFACE_PTR_TYPE((base_interface,
                                 default_delete<base_interface>))
      _i;
    } i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_TYPE_with_null_delete) {
  {
    class implementation {
    public:
      explicit implementation(base_fixture& f) : _i(new base_interface(f)) {
      }
      //
    private:
      UNIQUE_INTERFACE_PTR_TYPE((base_interface, null_delete)) _i;
    } i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_FRIEND_DECLARATION_with_default_deleter) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this)) {
          _i->check();
        }
        //
      private:
        UNIQUE_INTERFACE_PTR_FRIEND_DECLARATION(interface);
        UNIQUE_INTERFACE_PTR_TYPE(interface) _i;
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_FRIEND_DECLARATION_with_default_delete) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this)) {
          _i->check();
        }
        //
      private:
        UNIQUE_INTERFACE_PTR_FRIEND_DECLARATION((interface,
                                                 default_delete<interface>));
        UNIQUE_INTERFACE_PTR_TYPE((interface, default_delete<interface>))
        _i;
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_FRIEND_DECLARATION_with_null_delete) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this)) {
          _i->check();
        }
        //
      private:
        UNIQUE_INTERFACE_PTR_FRIEND_DECLARATION((interface, null_delete));
        UNIQUE_INTERFACE_PTR_TYPE((interface, null_delete)) _i;
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_OBJECT_DECLARATION_with_default_deleter) {
  {
    UNIQUE_INTERFACE_PTR_OBJECT_DECLARATION(base_interface, i)
    (new base_interface(*this));
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_OBJECT_DECLARATION_with_default_delete) {
  {
    UNIQUE_INTERFACE_PTR_OBJECT_DECLARATION(
        (base_interface, default_delete<base_interface>), i)
    (new base_interface(*this));
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_OBJECT_DECLARATION_with_null_delete) {
  {
    UNIQUE_INTERFACE_PTR_OBJECT_DECLARATION((base_interface, null_delete), i)
    (new base_interface(*this));
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_MEMBER_DECLARATION_with_default_deleter) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this)) {
          _i->check();
        }
        //
      private:
        UNIQUE_INTERFACE_PTR_MEMBER_DECLARATION(interface, _i);
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_MEMBER_DECLARATION_with_default_delete) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this)) {
          _i->check();
        }
        //
      private:
        UNIQUE_INTERFACE_PTR_MEMBER_DECLARATION((interface,
                                                 default_delete<interface>),
                                                _i);
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(1,  destructor_call_count);
  // clang-format on
}
//
T_TEST_CASE(UNIQUE_INTERFACE_PTR_MEMBER_DECLARATION_with_null_delete) {
  {
    class interface : public base_interface {
    public:
      class implementation {
      public:
        explicit implementation(base_fixture& f)
            : _i(new interface(f, this)) {
          _i->check();
        }
        //
      private:
        UNIQUE_INTERFACE_PTR_MEMBER_DECLARATION((interface, null_delete), _i);
      };
      //
    public:
      explicit interface(base_fixture& f, implementation* i)
          : base_interface(f), _i(i) {
      }
      //
    private:
      void
      check() {
        // clang-format off
        T_CHECK_EQ(_i->_i.get(),   this);
        T_CHECK_EQ(_i->_i->_i,     _i);
        T_CHECK_EQ(_i->_i->_i->_i, _i->_i);
        // ...
        // clang-format on
      }
      //
    private:
      implementation* _i;
    };
    interface::implementation i(*this);
  }
  // clang-format off
  T_CHECK_EQ(1, constructor_call_count);
  T_CHECK_EQ(0,  destructor_call_count);
  // clang-format on
}
//
T_TEST_SUITE_END
