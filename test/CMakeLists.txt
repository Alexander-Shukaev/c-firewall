### Preamble {{{
##  ==========================================================================
##        @file CMakeLists.txt
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-01-23 Monday 00:48:50 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-08-08 Monday 23:59:40 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
get_cxx11_standard_option(CXX11_STANDARD_OPTION REQUIRED)
if(CXX11_STANDARD_OPTION)
  list(APPEND PROJECT_OPTIONS ${CXX11_STANDARD_OPTION})
endif()
##
path(TEST_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR} include)
list(APPEND PROJECT_INCLUDE_DIRS ${TEST_INCLUDE_DIR})
##
add_subdirectory(source)
